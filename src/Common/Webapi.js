import 'whatwg-fetch';
const base_url = 'http://192.168.0.24:5055/tms/v1.0/api/';

export class Webapi {
    
    static send = (httpMethod, path, body, headers) => {
        return new Promise((resolve, reject) => {
            const myHeaders = new Headers();
            // myHeaders.append('Content-Type', 'application/json');
            // myHeaders.append("Access-Control-Allow-Origin", "*");
            // myHeaders.append("Access-Control-Allow-Credentials","true");         
            if (headers && headers.length > 0) {
                headers.forEach(element => {
                    myHeaders.append(element.key, element.value);
                });
            }

            return fetch(`${path}`, {
                method: httpMethod,
                // credentials: 'include',
                cors: 'true',
                headers: myHeaders,
                body: body ? JSON.stringify(body) : undefined
            }).then((response) => {
                if (!response.ok) { throw response; }
                return response.json();
            }).then((myJson, error) => {
                console.log(myJson);
                return resolve(myJson);
            }, (error) => {
                try {
                    return error.json().then(err => {
                        return reject(err);
                    })
                } catch (ex) {
                    return error;
                }
            });
        });
    }
    static sendWithToken = (httpMethod, path, body) => {
        var res = localStorage.getItem('res');
        if (res) {
            res = JSON.parse(res);
        }
        var authToken = res.authToken;
        var token = {
            key: "authToken", value: authToken
        }

        return Webapi.send(httpMethod, path, body, [token]);
    }



    parseJSON(response) {
        return new Promise((resolve, reject) => response.json()
            .then((json) => resolve({
                status: response.status,
                ok: response.ok,
                json,
            }))

        );
    }

}


