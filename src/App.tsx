import React from 'react';
import { Route, Switch, BrowserRouter, Redirect } from 'react-router-dom';
import './App.css';
import { Qform } from './components/form/form';
import { Home } from './components/home/home';
import { Bulk } from './components/bulk/bulkDSC';
import { BulkToken } from './components/bulk/bulkToken';
import { Downloads } from './components/home/downloads';
import { Footer } from './components/footer/footer';
import { Grid } from '@material-ui/core';


const renderNewDSCPage = (context: any) => {
  return <Qform type={"new_dsc"}/>;
}
const renderHomePage = (context: any) => {
  return <Home />;
}
const renderBulkPage = (context: any) => {
  return <Bulk />;
}
const renderBulkTokenPage = (context: any) => {
  return <BulkToken />;
}
const renderTechnicalQueryPage = (contect: any) => {
  return <Qform type={"technical_issue"}/>;
}
const renderDownloadsPage = (contect: any) => {
  return <Downloads />;
}
function App() {
  return (
    <div className="App">
      <div className="mini-footer">
          <div className="container">
          <div className="container">
          </div>
          <div className="row">
              <div className="col-md-12">
              <div className="copyright-text">
                  <p onClick={()=> window.location.href = "/home"}>RRSM MARKETING PRIVATE LIMITED</p>
              </div>
              </div>
          </div>
          </div>
      </div>
      <BrowserRouter>
          <Switch>
            <Route exact path="/home"
                component={(context: any) => renderHomePage(context)}  
              />
            <Route exact path="/technical-query"
                component={(context: any) => renderTechnicalQueryPage(context)}  
              />
            <Route exact path="/new-dsc"
              component={(context: any) => renderNewDSCPage(context)}  
            />
            <Route exact path="/bulk-dsc"
              component={(context: any) => renderBulkPage(context)}  
            />
            <Route exact path="/bulk-token"
              component={(context: any) => renderBulkTokenPage(context)}  
            />
            <Route exact path="/downloads"
              component={(context: any) => renderDownloadsPage(context)}  
            />
            <Redirect from="*" to="/home" />
            <Redirect from="/" to="/home" />
          </Switch>
        </BrowserRouter>
        <Footer />
    </div>
  );
}

export default App;
