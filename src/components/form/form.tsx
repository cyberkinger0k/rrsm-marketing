import React, { Component } from "react";
import TextField from '@material-ui/core/TextField';
import { Paper, Grid, FormControlLabel, Checkbox, FormControl, FormLabel, RadioGroup, Radio, FormGroup, Select, MenuItem, Button, InputLabel } from "@material-ui/core";
import "./form.css";
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import banner from  "./banner.jpg";
import axios from 'axios';
import { Webapi } from "../../Common/Webapi";
import Alert from "@material-ui/lab/Alert";

export interface QformState {
    form:any;
    error:any;
    files:any;
    buttonLoading:boolean;
    selectedType:any;
    docError:boolean;
    docErrorMsg:string;
    filesValidation:any;
    file_list: any;
}
export interface Qformprops {
    type:"technical_issue" | "new_dsc";
}
const organizationType = {
    "individual" :  [
        {name:"PAN Of Applicant", value:"pan_of_applicant"},
        {name:"AAdhar Card", value: "aadhar_card"},
        {name:"Photo of Applicant", value:"photo", type:"image"}
    ],
    "proprietor" : [
        {name:"PAN Of Applicant", value:"pan_of_applicant"},
        {name:"GST Certificate", value:"gst"},
        {name:"Photo of Applicant", value: "photo", type:"image"}

    ],
    "partnership_firm" : [
        {name:"PAN Of Applicant", value:"pan_of_applicant"},
        {name:"GST Certificate of firm", value:"gst"},
        {name:"Partnership Deed", value: "partnership_deed"},
        {name:"Photo of Applicant", value: "photo", type:"image"},
        {name:"PAN Of second partner", value:"pan_of_second_partner"},

    ],
    "company" : [
        {name:"PAN Of Applicant", value:"pan_of_applicant"},
        {name:"GST Certificate of company", value:"gst"},
        {name:"Photo of Applicant", value: "photo", type:"image"},
        {name:"PAN Of second director", value:"pan_of_second_director"},
        {name:"Board of resolution", value:"board_of_resolution"}

    ],
    "dgft_proprietor" : [
        {name:"PAN Of Applicant", value:"pan_of_applicant"},
        {name:"GST Certificate", value:"gst"},
        {name:"Photo of Applicant", value: "photo", type:"image"},
        {name:"IEC Certificate", value:"iec_certificate"},

    ],
    "dgft_partnership_firm" : [
        {name:"PAN Of Applicant", value:"pan_of_applicant"},
        {name:"GST Certificate of firm", value:"gst"},
        {name:"Partnership Deed", value: "partnership_deed"},
        {name:"Photo of Applicant", value: "photo", type:"image"},
        {name:"PAN Of second partner", value:"pan_of_second_partner"},
        {name:"IEC Certificate", value:"iec_certificate"},

    ],
    "dgft_company" : [
        {name:"PAN Of Applicant", value:"pan_of_applicant"},
        {name:"GST Certificate of company", value:"gst"},
        {name:"Photo of Applicant", value: "photo", type:"image"},
        {name:"PAN Of second director", value:"pan_of_second_director"},
        {name:"Board of resolution", value:"board_of_resolution"},
        {name:"IEC Certificate", value:"iec_certificate"},

    ],
    "dgft_llp" : [
        {name:"PAN Of Applicant", value:"pan_of_applicant"},
        {name:"GST Certificate of firm", value:"gst"},
        {name:"LLP deed", value:"llp_deed"},
        {name:"Photo of Applicant", value: "photo", type:"image"},
        {name:"PAN Of second partner", value:"pan_of_second_partner"},
        {name:"Board of resolution", value:"board_of_resolution"},
        {name:"IEC Certificate", value:"iec_certificate"},

    ],
    "sat" : [
        {name:"PAN Of Applicant", value:"pan_of_applicant"},
        {name:"GST Certificate of organisation", value:"gst"},
        {name:"Photo of Applicant", value: "photo", type:"image"},
        {name:"PAN of authorising person", value:"pan_of_authorising"},
        {name:"BYE-LAWS or trust deed", value:"trust_deed"}

    ],
    "foriegn" : [
        {name:"PASSPORT of applicant", value:"passport"},
        {name:"Address proof", value:"address_proof"},
        {name:"Photo of Applicant", value: "photo", type:"image"},
    ],
    "llp" : [
        {name:"Photo of Applicant", value: "photo"},
        {name:"GST Certificate of firm", value:"gst"},
        {name:"LLP deed", value:"llp_deed"},
        {name:"Photo of Applicant", value: "photo", type:"image"},
        {name:"PAN Of second partner", value:"pan_of_second_partner"},
        {name:"Board of resolution", value:"board_of_resolution"}


    ],
    "government" : [
        {name:"form with photo and sign", value:"form_with_photo_and_sign"},
        {name:"Gov. id card of applicant", value:"gov_id_card"},
        {name:"PAN Of Applicant", value:"pan_of_applicant"},
        {name:"Gov. id card of authorising person", value:"gov_id_card_of_authorising_person"},
    ],
    "bank" : [
        {name:"PAN Of Applicant", value:"pan_of_applicant"},
        {name:"ID card of applicant", value:"id_card_of_applicant"},
        {name:"PAN of bank", value:"pan_of_bank"},
        {name:"DSC form with photo", value:"dsc_form_with_photo"},
        {name:"Authorisation letter", value:"authorisation_letter"},
        {name:"ID card of authorising person", value:"id_card_of_authorising"},


    ],
}
const productsType = [
    // {name:"Class 2 sign", validity:"2 year", quantity: "1", isChecked:false},
    // {name:"Class 2 sign", validity:"3 year", quantity: "1", isChecked:false},
    // {name:"Class 2 combo", validity:"2 year", quantity: "1", isChecked:false},
    // {name:"Class 2 combo", validity:"3 year", quantity: "1", isChecked:false},
    {name:"Class 3 sign", validity:"2 year", quantity: "1", isChecked:false},
    {name:"Class 3 sign", validity:"3 year", quantity: "1", isChecked:false},
    {name:"Class 3 combo", validity:"2 year", quantity: "1", isChecked:false},
    {name:"Class 3 combo", validity:"3 year", quantity: "1", isChecked:false},
    {name:"DGFT", validity:"2 year", quantity: "1", isChecked:false},
    {name:"DGFT", validity:"3 year", quantity: "1", isChecked:false},
]
export class Qform extends Component<Qformprops, QformState> {

    constructor(props:any) {
        super(props);

        this.state = {
            form:{
                name:"",
                email:"",
                type: props.type,
            },
            files:[],
            file_list:[],
            filesValidation:[],
            error:{},
            buttonLoading: false,
            selectedType:[],
            docError: false,
            docErrorMsg: "",
        };
    }
    public render() {
        return (
            <div className="form-wrap">
                <header className="main-header" role="banner">
                <div className="crop-img">
                <img src={require('../logo.jpg')} alt="Banner Image"/>
                </div>
                </header>
                {/* <a href='https://www.freepik.com/vectors/banner'>Banner vector created by Harryarts - www.freepik.com</a> */}
                <form className={"form"} noValidate>
                <Paper style={{ padding: 40, borderRadius:10 }} className="card">
                <Grid xs={12} alignContent={"center"} className="form-heading"><h3>{this.props.type === "new_dsc"?"New Digital Signature":"Inquiry Form"}</h3></Grid>
                <Grid container spacing={2} direction="column" xs={12}>
                <Grid item xs={12}>
                <TextField  label="Name" name="name" variant="outlined" fullWidth size="small"
                value={this.state.form.name}
                onChange={this.updateFormFieldValue}
                error={this.state.error.nameError}
                helperText={this.state.error.nameError}/>
                </Grid>
                <Grid item xs={12}>
                <TextField label="Email" name="email" type="email" variant="outlined" fullWidth size="small"
                value={this.state.form.email}
                onChange={(e:any)=>this.updateFormFieldValue(e)}
                error={this.state.error.emailError}
                helperText={this.state.error.emailError}/>
                </Grid>
                <Grid item xs={12}>
                <TextField label="Mobile Number" name="phone" variant="outlined" fullWidth size="small"
                value={this.state.form.phone}
                onChange={(e)=>this.updateFormFieldValue(e)}
                error={this.state.error.mobileError}
                helperText={this.state.error.mobileError}/>
                </Grid>
                {/* <Grid item xs={12}>
                <TextField label="Type" name="type" variant="outlined" fullWidth size="small"
                value={this.state.form.type}
                onChange={(e)=>this.updateFormFieldValue(e)}
                error={this.state.error.typeError}
                helperText={this.state.error.typeError}/>
                </Grid> */}
                {this.state.form.type === "technical_issue" ?
                <Grid container spacing={2} direction="column" xs={12}>
                <Grid item xs={12}>
                <TextField label="Description" name="description" variant="outlined" fullWidth size="small"
                value={this.state.form.description}
                onChange={(e)=>this.updateFormFieldValue(e)}
                error={this.state.error.descriptionError}
                helperText={this.state.error.descriptionError}/>
                </Grid>
                <Grid item xs={12}>
                <Button
                variant={this.state.files.length?"outlined":"contained"}
                color={this.state.files.length?"primary":"default"}
                component="label"
                fullWidth
                >
                Upload Attachment / Screenshot
                <input
                    type="file"
                    accept=".jpeg"
                    name="screenshot"
                    onChange={(e)=>this.updateFormFieldValue(e)}
                    style={{ display: "none" }}
                />
                </Button>
                <div style={{color:"blue",float: "left",fontSize: "smaller"}}>{this.state.files.length?"attachment added":undefined}</div>
                </Grid>
                </Grid>:undefined}
                {this.state.form.type === "new_dsc"?
                <Grid container spacing={2} direction="column" xs={12}>
                <Grid item xs={12} lg={12}>
                <FormControl variant="outlined" fullWidth size="small">
                    <InputLabel htmlFor="outlined-age-native-simple">Organization Type</InputLabel>
                    <Select
                    name="organizationType"
                    native
                    value={this.state.form.organizationType}
                    onChange={(e)=>this.updateFormFieldValue(e)}
                    label="organisation"
                    inputProps={{
                        name: 'organizationType',
                    }}
                    >
                    <option value={""}></option>
                    <option value={"individual"}>Individual</option>
                    <option value={"proprietor"}>Proprietor</option>
                    <option value={"partnership_firm"}>Partnership Firm</option>
                    <option value={"company"}>Company</option>
                    <option value={"dgft_proprietor"}>DGFT purpose - Proprietor</option>
                    <option value={"dgft_partnership_firm"}>DGFT purpose - Partnership Firm</option>
                    <option value={"dgft_company"}>DGFT purpose - Company</option>
                    <option value={"dgft_llp"}>DGFT purpose - Limited Liability Partnership</option>
                    <option value={"sat"}>Society/Trust/AOP</option>
                    <option value={"foriegn"}>Foriegn National</option>
                    <option value={"llp"}>Limited Liability Partnership</option>
                    <option value={"government"}>Government</option>
                    <option value={"bank"}>Bank</option>
                    </Select>
                </FormControl>
                </Grid>
                <Grid item xs={12}>
                <TextField  label="Applicant Name" name="applicantName" variant="outlined" fullWidth size="small"
                value={this.state.form.applicantName}
                onChange={this.updateFormFieldValue}
                error={this.state.error.cnameError}
                helperText={this.state.error.cnameError}/>
                </Grid>
                <Grid item xs={12}>
                <TextField label="Applicant Email" name="applicantEmail" type="email" variant="outlined" fullWidth size="small"
                value={this.state.form.applicantEmail}
                onChange={(e:any)=>this.updateFormFieldValue(e)}
                error={this.state.error.cemailError}
                helperText={this.state.error.cemailError}/>
                </Grid>
                <Grid item xs={12}>
                <TextField label="Applicant Mobile Number" name="applicantPhone" variant="outlined" fullWidth size="small"
                value={this.state.form.applicantPhone}
                onChange={(e)=>this.updateFormFieldValue(e)}
                error={this.state.error.cmobileError}
                helperText={this.state.error.cmobileError}/>
                </Grid>
                {/* <Grid item xs={12}>
                <TextField label="Product Type" name="productType" variant="outlined" fullWidth size="small"
                value={this.state.form.productType}
                onChange={(e)=>this.updateFormFieldValue(e)}
                error={this.state.error.productTypeError}
                helperText={this.state.error.productTypeError}/>
                </Grid> */}
                <Grid item xs={12} lg={12}>
                <FormControl variant="outlined" fullWidth size="small">
                    <InputLabel htmlFor="outlined-age-native-simple">Product Type</InputLabel>
                    <Select
                    name="productType"
                    native
                    value={this.state.form.productType}
                    onChange={(e)=>this.updateFormFieldValue(e)}
                    label="Product Type"
                    inputProps={{
                        name: 'productType',
                    }}
                    >
                    <option value={""}></option>
                    {productsType.map(product=>{
                        return <option value={`${product.name} - ${product.validity}`}>{`${product.name} - ${product.validity}`}</option>
                    })}
                    </Select>
                </FormControl>
                </Grid>
                <Grid container spacing={2} xs={12}>
                {this.state.selectedType && this.state.selectedType.map((doc:any)=>{
                return(
                  <Grid item xs={12} lg={4}>
                  <Button
                  variant={(this.state.filesValidation.find((o:any) => o.name === doc.value))?"outlined":"contained"}
                  color={(this.state.filesValidation.find((o:any) => o.name === doc.value))?"primary":"default"}
                  component="label"
                  fullWidth
                  >
                  {doc.name}
                  <input
                      required
                      type="file"
                      accept={doc.type?".jpg, .jpeg, .png, .pdf":".pdf"}
                      name={doc.value}
                      onChange={(e)=>this.updateFormFieldValue(e)}
                      style={{ display: "none" }}
                  />
                  </Button>
                  <div style={{color:"blue",float: "left",fontSize: "smaller"}}>{(this.state.filesValidation.find((o:any) => o.name === doc.value))?"attachment added":undefined}</div>
                  </Grid>)  
                })}
                </Grid>
                </Grid>
                :undefined}
                <Grid item style={{ marginTop: 16 }}>
                  <Button
                    variant="contained"
                    color="primary"
                    disabled={this.state.buttonLoading}
                    onClick={this.onSubmit}
                    fullWidth 
                  >
                    {this.state.buttonLoading?"Loading...":"Submit"}
                  </Button>
                </Grid>
              </Grid>
              <Snackbar open={this.state.docError} anchorOrigin={{ vertical: 'top', horizontal: 'center' }} autoHideDuration={6000} onClose={this.handleClose}>
                        <Alert onClose={this.handleClose} severity="error">
                        {this.state.docErrorMsg}
                        </Alert>
              </Snackbar>
            </Paper>
        </form>
    </div>
    );}
    private handleClose = () => {
        this.setState({ docError: false });
    }
    private onSubmit = async () => {
        console.log("selectedType",this.state.selectedType)
        console.log("fileValidation",this.state.filesValidation);
        this.setState({ buttonLoading: true});
        const state = this.state;
        let error = state.error;
        let SubmitStatus = true;
        if(!state.form.name){
           error.nameError ="please enter your name";
           SubmitStatus = false;
        }else{
            error.nameError ="";
        }
        if(!state.form.email || !this.validateEmail(state.form.email)){
            error.emailError ="please enter valid email"; 
            SubmitStatus = false;
         }else{
             error.emailError =""; 
         }
         if(!state.form.phone || isNaN(state.form.phone) || (state.form.phone.length > 11 && state.form.phone.length < 9)){
            error.mobileError ="please enter valid mobile number"; 
            SubmitStatus = false;
         }else{
             error.mobileError =""; 
         }
         if(state.form.type === "technical_issue"){
            if(!state.form.description){
                error.descriptionError ="please enter description";
                SubmitStatus = false;
             }else{
                 error.mobileError =""; 
             }
            //  if(!state.form.screenshot){
            //     this.setState({ docError: true, docErrorMsg:`please enter screenshot`});
            //     SubmitStatus = false;
            //  }else{
            //      error.mobileError =""; 
            //  }
         }
         if(state.form.type === "new_dsc"){
            if(!state.form.applicantName){
                error.cnameError ="please enter your name";
                SubmitStatus = false;
             }else{
                 error.cnameError ="";
             }
             if(!state.form.applicantEmail || !this.validateEmail(state.form.applicantEmail)){
                 error.cemailError ="please enter valid email"; 
                 SubmitStatus = false;
              }else{
                  error.cemailError =""; 
              }
              if(!state.form.applicantPhone || isNaN(state.form.applicantPhone) || (state.form.applicantPhone.length > 11 && state.form.applicantPhone.length < 9)){
                 error.cmobileError ="please enter valid mobile number"; 
                 SubmitStatus = false;
              }else{
                  error.cmobileError =""; 
              }
              if(!state.form.productType){
                error.productTypeError ="please enter your product type";
                SubmitStatus = false;
             }else{
                 error.productTypeError ="";
             }
         }
         if(state.form.type === "new_dsc"){
            if(state.files.length === 0){
                this.setState({ docError: true, docErrorMsg:`Enter all documents`});
                SubmitStatus = false;
            }else if(state.files.length !== state.selectedType.length){
                this.setState({ docError: true, docErrorMsg:`Enter all documents`});
                SubmitStatus = false;
            }
        }
         this.setState({ error });
         console.log(state.files);
         if(!SubmitStatus){
            this.setState({ buttonLoading: false});
            return;
        }
        let headers = { 'Content-Type': `multipart/form-data; charset=utf-8; boundary= + ${Math.random().toString().substr(2)}`,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": "true" };
        // const filesList = await this.state.files.filter((v:any,i:any,a:any)=>a.findIndex((t:any)=>(JSON.stringify({...t}) === JSON.stringify({...v})))===i);
        // console.log(filesList);
        // return;
        let filesList:any = this.state.files;
        let files_list:any = [];
        let file:any = [];
        files_list = filesList.map((files:any,index:any)=>{
                return this.formAxios.post('http://ec2-13-232-243-155.ap-south-1.compute.amazonaws.com/v1/api/upload/file',{...files}, {headers})
                .then(res => {
                    console.log(res);
                    file.push(res.data.fileName);
                }).catch(err => {alert("Something went wrong while uploading please try again !!");});
            });
        Promise.all(files_list).then(res=>{
            const reqBody = {
                ...this.state.form,
                heading: this.state.form.type === "new_dsc"?"We'd like to let you know that we have received your application for a new digital signature. Our team will process the application and notify you of the application number and video link. Kindly complete video verification using video link.":"We'd like to let you know that we have received your technical issue. Our technical team will contact you shortly.",
                subject: this.state.form.type === "new_dsc"? "New Digital Signature Required":"Technical Issue",
                "files":file,
            }
            console.log("fileList1",file);
            console.log("requestBody",reqBody)
        axios.post('http://ec2-13-232-243-155.ap-south-1.compute.amazonaws.com/v1/api/mail',{...reqBody}).then(response => {
                console.log(response);
                this.setState({ form:{}, files:{} });
                this.setState({ buttonLoading: false});
                alert("submitted successfully !!");
                window.location.reload();
            }).catch(error => {
                this.setState({ buttonLoading: false});
                console.log(error);
            });
          })
    }
    //put formAxios in its own module to reuse it across the project
    private formAxios = axios.create({
        transformRequest: [function (data, headers) {
            if (headers['Content-Type'] && headers['Content-Type'].startsWith('multipart/form-data')) {
                const form = new FormData();
                for (const key in data) {
                    const value = data[key];
                    if (Array.isArray(value)) {
                        const arrayKey = `${key}[]`;
                        value.forEach(v => {
                            form.append("file", v);
                        });
                    } else{
                        form.append("file", value);
                    }
                }
                return form;
            }

            return data;
        }],
    });  
    private validateEmail(email:string) {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    private updateFormFieldValue = (e:any) => {
        console.log((this.state.filesValidation.find((o:any) => o.name === "name")));
        console.log(this.state.filesValidation);
        const state = this.state;
        let selectedType = this.state.selectedType;
        let form = state.form;
        let files = state.files;
        let filesValidation = state.filesValidation;
        if(e.target.files){
            // let fileFormat = {
            //     lastModified: e.target.files[0].lastModified,
            //     lastModifiedDate: e.target.files[0].lastModifiedDate,
            //     name: e.target.name,
            //     size: e.target.files[0].size,
            //     type: e.target.files[0].type,
            //     webkitRelativePath: e.target.files[0].webkitRelativePath,
            // }
            files = [
                ...files,
                {[e.target.name]: e.target.files[0]},
            ];
            filesValidation = [
                ...filesValidation,
                {"name":e.target.name},
            ]
        }else{
        form = {
            ...form,
            [e.target.name]: e.target.value,
        };
        }
        if(e.target.name === "organizationType"){
        console.log("hey");
        files = [];
        filesValidation = [];
        for (const prop in organizationType) {
            if(prop === e.target.value){
                console.log(prop);
                switch(prop) {
                    case "individual":
                        selectedType = organizationType.individual;
                        
                        break;
                    case "proprietor":
                        selectedType = organizationType.proprietor;
                        
                        break;
                    case "partnership_firm":
                        selectedType = organizationType.partnership_firm;
                        
                        break;
                    case "company":
                        selectedType = organizationType.company;
                        
                        break;
                    case "dgft_proprietor":
                        selectedType = organizationType.dgft_proprietor;
                        
                        break;
                    case "dgft_partnership_firm":
                        selectedType = organizationType.dgft_partnership_firm;
                        
                        break;
                    case "dgft_company":
                        selectedType = organizationType.dgft_company;
                        
                        break;
                    case "dgft_llp":
                        selectedType = organizationType.dgft_llp;
                        
                        break;
                    case "sat":
                        selectedType = organizationType.sat;
                        
                        break;
                    case "foriegn":
                        selectedType = organizationType.foriegn;
                        
                        break;
                    case "llp":
                        selectedType = organizationType.llp;
                        
                        break;
                    case "government":
                        selectedType = organizationType.government;
                        
                        break;
                    case "bank":
                        selectedType = organizationType.bank;
                        
                        break;
                    default:
                      // code block
                  }
            }
          }
        }
        console.log("selected",selectedType);
        this.setState({ form: form, files: files,filesValidation, selectedType });
        console.log(form);
        console.log(files);
    }
    private removeDuplicates(originalArray:[], prop:any) {
        var newArray = [];
        var lookupObject:any  = {};
   
        for(var i in originalArray) {
           lookupObject[originalArray[i][prop]] = originalArray[i];
        }
   
        for(i in lookupObject) {
            newArray.push(lookupObject[i]);
        }
         return newArray;
    }
}