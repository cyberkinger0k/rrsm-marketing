import React, { Component } from "react";
import "./footer.css";


export class Footer extends Component<{},{}> {

    constructor(props:any) {
        super(props);

        
    }
    public render() {
        return (
            <footer className="footer-area footer--light">
                <div className="footer-big">
                    <div className="container">
                    <div className="row">
                        <div className="col-md-3 col-sm-12">
                        <div className="footer-widget">
                            <div className="widget-about">
                            <p>RRSM MARKETING PRIVATE LIMITED <br />
                            HEAD OFFICE/ CORPORATE OFFICE: - 207, Aggarwal Chamber 4, 27, Veer Savarkar Block, Shakarpur, Delhi 110091 <br />
                            BRANCH OFFICE: c/o Mohit Kansal, Ist Floor, 116, opposite SBI, Navyug Market, Ghaziabad 201001, UP
                            </p>
                            <ul className="contact-details">
                                <li>
                                <span className="icon-earphones">Call us:</span><br />
                                <a href="tel:01204331829">01204331829</a><span/>
                                <a href="tel:01204119129">01204119129</a><span/> 
                                <a href="tel:9548699991">9548699991</a><span/>
                                </li>
                                <li>
                                <span className="icon-envelope-open">Contact us:</span><br />
                                <a href="mailto:mohitkansal8@gmail.com">mohitkansal8@gmail.com</a>
                                </li>
                            </ul>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>

                <div className="mini-footer">
                    <div className="container">
                    <div className="container">
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                        <div className="copyright-text">
                            <p>RRSM MARKETING PRIVATE LIMITED All rights reserved. Created by CyberKing</p>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </footer>
    );}
}