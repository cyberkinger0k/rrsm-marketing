import React, { Component } from "react";
import TextField from '@material-ui/core/TextField';
import { Paper, Grid, MenuItem, Button, InputLabel, Fade, Checkbox, List, ListItem, ListItemAvatar, Avatar, ListItemText, Typography, Divider, FormControl, Select, Snackbar } from "@material-ui/core";
import Menu, { MenuProps } from '@material-ui/core/Menu';
import PopupState, { bindTrigger, bindMenu } from 'material-ui-popup-state';
import { Redirect } from "react-router-dom";
import axios from 'axios';
import Alert from "@material-ui/lab/Alert";
import { resolve } from "url";

export interface BulkState {
    products: any;
    form:any;
    error:any;
    docError:boolean;
    docErrorMsg:string;
    buttonLoading: boolean;
    file:any;
}

export class Bulk extends Component<{}, BulkState> {

    constructor(props:any) {
        super(props);

        this.state = {
           products:[
            // {name:"Class 2 sign", validity:"2 year", quantity: "1", isChecked:false},
            // {name:"Class 2 sign", validity:"3 year", quantity: "1", isChecked:false},
            // {name:"Class 2 combo", validity:"2 year", quantity: "1", isChecked:false},
            // {name:"Class 2 combo", validity:"3 year", quantity: "1", isChecked:false},
            {name:"Class 3 sign", validity:"2 year", quantity: "1", isChecked:false},
            {name:"Class 3 sign", validity:"3 year", quantity: "1", isChecked:false},
            {name:"Class 3 combo", validity:"2 year", quantity: "1", isChecked:false},
            {name:"Class 3 combo", validity:"3 year", quantity: "1", isChecked:false},
            {name:"DGFT", validity:"2 year", quantity: "1", isChecked:false},
            {name:"DGFT", validity:"3 year", quantity: "1", isChecked:false},
        ],
        form:{},
        file:{},
        error:{},
        docError:false,
        docErrorMsg:"",
        buttonLoading:false,
        };
    }
    public render() {
        const state = this.state;
        const productList = state.products;
        return (
            <div className="form-wrap">
                <header className="main-header" role="banner">
                <div className="crop-img">
                <img src={require('../logo.jpg')} alt="Banner Image"/>
                </div>
                </header>
                <Paper style={{ padding: 40, borderRadius:10, width: "76vw" }} className="card">
                <Grid xs={12} alignContent={"center"} className="form-heading"><h3>Bulk Order - Digital Signature</h3></Grid>
                <Grid container spacing={2} direction="column" xs={12}>
                <Grid item xs={12}>
                <TextField  label="Name" name="name" variant="outlined" fullWidth size="small"
                value={this.state.form.name}
                onChange={this.updateFormFieldValue}
                error={this.state.error.nameError}
                helperText={this.state.error.nameError}/>
                </Grid>
                <Grid item xs={12}>
                <TextField label="Mobile Number" name="phone" variant="outlined" fullWidth size="small"
                value={this.state.form.phone}
                onChange={(e)=>this.updateFormFieldValue(e)}
                error={this.state.error.mobileError}
                helperText={this.state.error.mobileError}/>
                </Grid>
                <Grid item xs={12}>
                <TextField label="Email" name="email" type="email" variant="outlined" fullWidth size="small"
                value={this.state.form.email}
                onChange={(e:any)=>this.updateFormFieldValue(e)}
                error={this.state.error.emailError}
                helperText={this.state.error.emailError}/>
                </Grid>
                <Grid item xs={12}>
                <FormControl variant="outlined" fullWidth size="small">
                    <InputLabel htmlFor="outlined-age-native-simple">Company</InputLabel>
                    <Select
                    name="company"
                    native
                    value={this.state.form.company}
                    onChange={(e)=>this.updateFormFieldValue(e)}
                    label="company"
                    inputProps={{
                        name: 'company',
                        id: 'outlined-age-native-simple',
                    }}
                    >
                    <option aria-label="None" value="" />
                    <option value={"emudhra"}>Emudhra</option>
                    <option value={"v-sign"}>V-sign</option>
                    <option value={"pantasign"}>Pantasign</option>
                    <option value={"capricorn"}>Capricorn</option>
                    </Select>
                </FormControl>
                </Grid>
                <List dense={true}>
                <Grid container spacing={2}>
                {productList.map((product:any,index:number)=>{
                return(
                <Grid item xs={12} lg={6}>
                <ListItem alignItems="flex-start" className={product.isChecked?"selected":""}>
                    <ListItemAvatar>
                    <Checkbox
                        name="isChecked"
                        edge="start"
                        color="primary"
                        value={product.isChecked}
                        onChange={(e)=>this.onChange(index,e)}
                        tabIndex={-1}
                        disableRipple
                    />
                    </ListItemAvatar>
                    <ListItemText
                    primary={<React.Fragment><strong>{product.name} - {product.validity}</strong></React.Fragment>}
                    secondary={
                        <React.Fragment>
                        <Grid container spacing={2}>
                        <Grid item xs={6} md={4}>
                            <TextField
                                disabled={!product.isChecked}
                                name="quantity"
                                type="number"
                                size="small"
                                variant="outlined"
                                value={product.isChecked?product.quantity:0}
                                onChange={(e)=>this.onChange(index,e)}
                                InputProps={{
                                    inputProps: { 
                                        min: 1 
                                    }
                                }}
                                label="Quantity"
                            />
                        </Grid>
                        <Grid item xs={6} md={4}>
                            <TextField 
                                disabled={!product.isChecked}
                                name="price"
                                type="number"
                                size="small"
                                variant="outlined"
                                onChange={(e)=>this.onChange(index,e)}
                                InputProps={{
                                    inputProps: { 
                                         min: 10 
                                    }
                                }}
                                label="price"
                            />
                        </Grid>
                        </Grid>
                        </React.Fragment>
                    }
                    />
                </ListItem>
                <Divider variant="inset" component="li" />
                </Grid>
                )})}
                <Grid item xs={12}>
                <TextField label="Login Id" name="loginId" variant="outlined" fullWidth size="small"
                value={this.state.form.loginId}
                onChange={(e:any)=>this.updateFormFieldValue(e)}
                error={this.state.error.loginIdError}
                helperText={this.state.error.loginIdError}/>
                </Grid>
                <Grid item xs={12}>
                <Button
                variant={this.state.file.name?"outlined":"contained"}
                color={this.state.file.name?"primary":"default"}
                component="label"
                fullWidth
                >
                {"Attach Screenshot if payment already done"}
                <input
                    type="file"
                    name="screenshot"
                    onChange={(e)=>this.updateScreenshot(e)}
                    style={{ display: "none" }}
                />
                </Button>
                <div style={{color:"blue",float: "left",fontSize: "smaller"}}>{this.state.file.name?"attachment added":undefined}</div>
                </Grid>
                <Grid item style={{ marginTop: 16 }} xs={12}>
                  <Button
                    variant="contained"
                    color="primary"
                    disabled={this.state.buttonLoading}
                    onClick={this.onSubmit}
                    fullWidth 
                  >
                    {this.state.buttonLoading?"Loading...":"Submit"}
                  </Button>
                </Grid>
                </Grid>
                </List>
                </Grid>
                <Snackbar open={this.state.docError} anchorOrigin={{ vertical: 'top', horizontal: 'center' }} autoHideDuration={6000} onClose={this.handleClose}>
                        <Alert onClose={this.handleClose} severity="error">
                        {this.state.docErrorMsg}
                        </Alert>
                </Snackbar>
                </Paper>
            </div>
    );}
    private updateScreenshot = (e: any) => {
        this.setState({ file: e.target.files[0]});
    }
    private handleClose = () => {
        this.setState({ docError: false });
    }
    private onChange = (index:number,e:any) => {
        let productList = this.state.products;
        if(e.target.name === "isChecked"){
            productList[index] = {...productList[index], [e.target.name]:e.target.checked};
        }else{
            productList[index] = {...productList[index], [e.target.name]:e.target.value};
        }
        this.setState({ products: productList });
    }
    private updateFormFieldValue = (e:any) => {
        let form = this.state.form;
        form = {
            ...form,
            [e.target.name]: e.target.value,
        };
        this.setState({ form });
    }
    private onSubmit = () => {
        const state = this.state;
        let error = state.error;
        let SubmitStatus = true;
        let productList:any = [];
        this.setState({ buttonLoading: true });
        state.products.map((product:any)=> {
            if(product.isChecked === true){
             productList.push(product);
            }
        });
        if(!state.form.name){
           error.nameError ="please enter your name";
           SubmitStatus = false;
        }else{
            error.nameError ="";
        }
        if(!state.form.email || !this.validateEmail(state.form.email)){
            error.emailError ="please enter valid email"; 
            SubmitStatus = false;
         }else{
             error.emailError =""; 
         }
         if(!state.form.phone || isNaN(state.form.phone) || (state.form.phone.length > 11 && state.form.phone.length < 9)){
            error.mobileError ="please enter valid mobile number"; 
            SubmitStatus = false;
         }else{
             error.mobileError =""; 
         }
         if(!state.form.loginId){
            error.loginIdError ="please enter your login id";
            SubmitStatus = false;
         }else{
             error.loginIdError ="";
         }
         if(!state.form.company){
            this.setState({ docError: true, docErrorMsg:`Enter company name`});
            SubmitStatus = false;
         }
         if(!productList.length){
            this.setState({ docError: true, docErrorMsg:`Please select product`});
            SubmitStatus = false;
         }
         this.setState({ error });
         if(!SubmitStatus){
            this.setState({ buttonLoading: false});
            return;
        }
          let headers = { 'Content-Type': `multipart/form-data; charset=utf-8; boundary= + ${Math.random().toString().substr(2)}`,
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Credentials": "true" };
          // const filesList = await this.state.files.filter((v:any,i:any,a:any)=>a.findIndex((t:any)=>(JSON.stringify({...t}) === JSON.stringify({...v})))===i);
          // console.log(filesList);
          // return;
          let filesList:any = this.state.file;
          let files_list:any = [];
          let file:any;
          files_list = [ this.state.file.name && 
            this.formAxios.post('http://ec2-13-232-243-155.ap-south-1.compute.amazonaws.com/v1/api/upload/file',{filesList}, {headers})
                  .then(res => {
                      console.log(res);
                      file = res.data.fileName;
                  }).catch(err => {alert("screenshot not uploaded !!");})
                  ]
          Promise.all(files_list).then(res=>{
              const reqBody = {
                  ...this.state.form,
                  isTokenOrder: false,
                  heading: "We'd like to let you know that the following item(s) in your order will be transferred in your login account, we will reply you notifying the same.",
                  subject: `Confirmation email for digital signature order for ${this.state.form.company}`,
                  products: productList,
                  "file": file,
              }
              console.log("fileList1",file);
              console.log("requestBody",reqBody)
          axios.post('http://ec2-13-232-243-155.ap-south-1.compute.amazonaws.com/v1/api/order/mail',{...reqBody}).then(response => {
                  console.log(response);
                  this.setState({ buttonLoading: false});
                  alert("submitted successfully !!");
                  window.location.reload();
              }).catch(error => {
                  this.setState({ buttonLoading: false});
                  console.log(error);
              });
            })
    }
    //put formAxios in its own module to reuse it across the project
    private formAxios = axios.create({
        transformRequest: [function (data, headers) {
            if (headers['Content-Type'] && headers['Content-Type'].startsWith('multipart/form-data')) {
                const form = new FormData();
                for (const key in data) {
                    const value = data[key];
                    if (Array.isArray(value)) {
                        const arrayKey = `${key}[]`;
                        value.forEach(v => {
                            form.append("file", v);
                        });
                    } else{
                        form.append("file", value);
                    }
                }
                return form;
            }

            return data;
        }],
    });
    private validateEmail(email:string) {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    
}