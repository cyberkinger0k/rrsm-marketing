import React, { Component } from "react";
import { Paper, Grid, MenuItem, Button, InputLabel, Fade } from "@material-ui/core";
import Menu, { MenuProps } from '@material-ui/core/Menu';
import PopupState, { bindTrigger, bindMenu } from 'material-ui-popup-state';

export interface homeState {
    bulkMenu: boolean
}
export class Home extends Component<{}, homeState> {

    constructor(props:any) {
        super(props);

        this.state = {
           bulkMenu: false,
        };
    }
    public render() {
        return (
            <div className="form-wrap">
                <header className="main-header" role="banner">
                <div className="crop-img">
                <img src={require('../logo.jpg')} alt="Banner Image"/>
                </div>
                </header>
                <Paper style={{ padding: 40, borderRadius:10 }} className="card">
                <Grid container spacing={2} xs={12}>
                    <Grid item xs={12}>
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={()=> window.location.href = "/new-dsc"}
                            fullWidth 
                        >NEW Digital Signature
                        </Button>
                    </Grid>
                    <Grid item xs={12}>
                    <PopupState variant="popover" popupId="demo-popup-menu">
                    {(popupState) => (
                        <React.Fragment>
                        <Button variant="contained" color="primary" fullWidth {...bindTrigger(popupState)}>
                            Bulk Order
                        </Button>
                        <Menu {...bindMenu(popupState)}>
                            <MenuItem onClick={()=> window.location.href = "/bulk-dsc"}>Digital signature</MenuItem>
                            <MenuItem onClick={()=> window.location.href = "/bulk-token"}>USB token</MenuItem>
                        </Menu>
                        </React.Fragment>
                    )}
                    </PopupState>
                    </Grid>
                    <Grid item xs={12}>
                    <Button
                        variant="contained"
                        color="primary"
                        fullWidth 
                        onClick={()=> window.location.href = "/technical-query"}
                    >Technical Issue
                    </Button>
                    </Grid>
                    <Grid item xs={12}>
                        <Button
                            variant="contained"
                            color="primary"
                            fullWidth
                            onClick={()=> window.location.href = "/downloads"}
                        >Downloads
                        </Button>
                    </Grid>
                </Grid>
                </Paper>
            </div>
    );}
}