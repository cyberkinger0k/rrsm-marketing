import React, { Component } from "react";
import TextField from '@material-ui/core/TextField';
import { Paper, Grid, MenuItem, Button, InputLabel, Fade, ListItem, ListItemText } from "@material-ui/core";
import Menu, { MenuProps } from '@material-ui/core/Menu';
import PopupState, { bindTrigger, bindMenu } from 'material-ui-popup-state';
import { Redirect } from "react-router-dom";


export class Downloads extends Component<{}, {}> {

    constructor(props:any) {
        super(props);
    }
    public render() {
        return (
            <div className="form-wrap">
                <header className="main-header" role="banner">
                <div className="crop-img">
                <img src={require('../logo.jpg')} alt="Banner Image"/>
                </div>
                </header>
                <Paper style={{ padding: 40, borderRadius:10, width:"70vw" }} className="card">
                <Grid container spacing={2} xs={12}>
                    <Grid item xs={12}><h1>Download Links</h1></Grid>
                    <Grid item xs={12}>
                        <h3>organisation</h3>
                    </Grid>
                    <Grid item xs={12}>
                        <a href="https://www.e-mudhra.com/Repository/Forms/Signature-Encryption-Organization-Printable-v3.2.pdf">eMudhra</a>
                    </Grid>
                    <Grid item xs={12}>
                        <a href="https://www.pantasign.com/forms/application_Form.pdf">Pantasign</a>
                    </Grid>
                    <Grid item xs={12}>
                        <a href="https://www.vsign.in/forms/Offline-Organisation.pdf">Vsign</a>
                    </Grid>
                    <Grid item xs={12}>
                        <a href="https://www.certificate.digital/repository/forms/organization-form.pdf">Capricorn</a>
                    </Grid>
                    <Grid item xs={12}><hr className="dashed"></hr></Grid>

                    <Grid item xs={12}>
                    <h3>DGFT</h3>
                    </Grid>
                    <Grid item xs={12}>
                    <a href="https://www.e-mudhra.com/Repository/Forms/Signature-DGFT-Printable-v3.2.pdf">eMudhra</a>
                    </Grid>
                    <Grid item xs={12}>
                    <a href="https://www.pantasign.com/forms/application_Form.pdf">Pantasign</a>
                    </Grid>
                    <Grid item xs={12}>
                    <a href="https://www.vsign.in/forms/Offline-DGFT.pdf">Vsign</a>
                    </Grid>
                    <Grid item xs={12}>
                        <a href="https://www.certificate.digital/repository/forms/IET-form.pdf">Capricorn</a>
                    </Grid>
                    <Grid item xs={12}><hr className="dashed"></hr></Grid>

                    <Grid item xs={12}>
                    <h3>Government</h3>
                    </Grid>
                    <Grid item xs={12}>
                    <a href="https://www.e-mudhra.com/Repository/Forms/Signature-Encryption-Government-Printable-v2.9.pdf">eMudhra</a>
                    </Grid>
                    <Grid item xs={12}>
                    <a href="https://www.pantasign.com/forms/Pantasign_Organization_Government.pdf">Pantasign</a>
                    </Grid>
                    <Grid item xs={12}>
                    <a href="https://www.vsign.in/forms/Offline-Organisation-Government.pdf">Vsign</a>
                    </Grid>
                    <Grid item xs={12}>
                        <a href="https://www.certificate.digital/repository/forms/dsc-for-gov-organization.pdf">Capricorn</a>
                    </Grid>
                    <Grid item xs={12}><hr className="dashed"></hr></Grid>

                    <Grid item xs={12}>
                    <h3>Bank</h3>
                    </Grid>
                    <Grid item xs={12}>
                    <a href="https://www.e-mudhra.com/Repository/Forms/Signature-Encryption-Banks-Printable-v2.9.pdf">eMudhra</a>
                    </Grid>
                    <Grid item xs={12}>
                    <a href="https://www.pantasign.com/forms/Panta_Organization_Bank.pdf">Pantasign</a>
                    </Grid>
                    <Grid item xs={12}>
                    <a href="https://www.vsign.in/forms/Offline-Organisation-Bank.pdf">Vsign</a>
                    </Grid>
                    <Grid item xs={12}><hr className="dashed"></hr></Grid>

                    <Grid item xs={12}>
                    <h3>Letter of Identity</h3>
                    </Grid>
                    <Grid item xs={12}>
                    <a href="https://www.e-mudhra.com/Repository/Templates/Template-eMudhra-ID-Proof-Letter.pdf">eMudhra</a>
                    </Grid>
                    <Grid item xs={12}>
                    <a href="https://www.pantasign.com/forms/id-proof-letter.pdf">Pantasign</a>
                    </Grid>
                    <Grid item xs={12}><hr className="dashed"></hr></Grid>

                    <Grid item xs={12}>
                    <h3>Board of Resolution</h3>
                    </Grid>
                    <Grid item xs={12}>
                    <a href="https://www.e-mudhra.com/Repository/Templates/Template-eMudhra-Authorization-Letter.pdf">eMudhra</a>
                    </Grid>
                    <Grid item xs={12}>
                    <a href="https://www.pantasign.com/forms/board-resolution.pdf">Pantasign</a>
                    </Grid>
                    <Grid item xs={12}>
                    <a href="https://www.vsign.in/authority-letter.html">Vsign</a>
                    </Grid>
                    <Grid item xs={12}><hr className="dashed"></hr></Grid>
                </Grid>
                </Paper>
            </div>
    );}
}